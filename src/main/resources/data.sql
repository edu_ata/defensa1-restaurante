delete from category;
delete from restaurant;
delete from ciudad;
delete from level_price;


INSERT INTO category(id, name) VALUES (1000, 'Desayuno');
INSERT INTO category(id, name) VALUES (1001, 'Almuerzo');
INSERT INTO category(id, name) VALUES (1002, 'Cena Familiar');

INSERT INTO restaurant(id, name,category_id,descripcion,foto,ubicacion) VALUES (1001, 'HyperMaxi',1000,'buffet','foto1','cbba');
INSERT INTO restaurant(id, name,category_id,descripcion,foto,ubicacion) VALUES (1002, 'IceNorte',1001,'almuerzos','foto2','cbba');
INSERT INTO restaurant(id, name,category_id,descripcion,foto,ubicacion) VALUES (1003, 'Planchitas',1002,'platos variados','foto3','cbba');

INSERT INTO ciudad(id, name) VALUES (10, 'La Paz');
INSERT INTO ciudad(id, name) VALUES (11, 'Cochabamba');
INSERT INTO ciudad(id, name) VALUES (12, 'Santa Cruz');

INSERT INTO level_price(id, descripcion) VALUES (10, 'Economico');
INSERT INTO level_price(id, descripcion) VALUES (11, 'Moderado');
INSERT INTO level_price(id, descripcion) VALUES (12, 'Lujo');