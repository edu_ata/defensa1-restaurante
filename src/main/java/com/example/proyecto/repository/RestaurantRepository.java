package com.example.proyecto.repository;

import com.example.proyecto.entities.Restaurant;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface RestaurantRepository extends CrudRepository<Restaurant,Integer>{
}
