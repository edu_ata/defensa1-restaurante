package com.example.proyecto.repository;

import com.example.proyecto.entities.Ciudad;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional

public interface CiudadRepository extends CrudRepository<Ciudad,Integer>{
}
