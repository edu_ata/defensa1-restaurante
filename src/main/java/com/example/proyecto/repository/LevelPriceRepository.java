package com.example.proyecto.repository;

import com.example.proyecto.entities.LevelPrice;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional

public interface LevelPriceRepository extends CrudRepository<LevelPrice,Integer>{
}