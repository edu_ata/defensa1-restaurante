package com.example.proyecto.services;
import com.example.proyecto.entities.Restaurant;

public interface RestaurantService {

    Iterable<Restaurant> listAllRestaurants();
    void saveRestaurant(Restaurant restaurant);
    Restaurant getRestaurant(Integer id);
    void deleteRestaurant(Integer id);

}
