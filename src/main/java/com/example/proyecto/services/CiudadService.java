package com.example.proyecto.services;


import com.example.proyecto.entities.Ciudad;

public interface CiudadService {
    Iterable<Ciudad> listAllCiudad();

    void saveCiudad(Ciudad ciudad);

    Ciudad getCiudad(Integer id);

    void deleteCiudad(Integer id);
}
