package com.example.proyecto.services;

import com.example.proyecto.entities.LevelPrice;
import com.example.proyecto.repository.LevelPriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class LevelPriceServiceImpl implements LevelPriceService {

    private LevelPriceRepository levelPriceRepository;

    @Autowired
    @Qualifier(value="LevelPriceRepository")
    public void setLevelPriceRepository(LevelPriceRepository levelPriceRepository) {
        this.levelPriceRepository = levelPriceRepository;
    }

    @Override
    public Iterable<LevelPrice> listAllLevelprice() {
        return levelPriceRepository.findAll();
        //return ciudadRepository.findAll();
    }

    @Override
    public void saveLevelPrice(LevelPrice levelPrice) {
        //ciudadRepository.save(ciudad);
        levelPriceRepository.save(levelPrice);
    }

    @Override
    public LevelPrice getLevelprice(Integer id) {
        return levelPriceRepository.findById(id).get();
    }

    @Override
    public void deleteLevelprice(Integer id) {
        //ciudadRepository.deleteById(id);
        levelPriceRepository.deleteById(id);
    }



}
