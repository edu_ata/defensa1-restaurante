package com.example.proyecto.services;

import com.example.proyecto.entities.LevelPrice;

public interface LevelPriceService {
    Iterable<LevelPrice> listAllLevelprice();

    void saveLevelPrice(LevelPrice levelPrice);

    LevelPrice getLevelprice(Integer id);

    void deleteLevelprice(Integer id);
}
