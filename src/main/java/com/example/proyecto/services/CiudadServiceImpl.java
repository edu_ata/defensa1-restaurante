package com.example.proyecto.services;

import com.example.proyecto.entities.Category;
import com.example.proyecto.entities.Ciudad;
import com.example.proyecto.repository.CiudadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class CiudadServiceImpl implements CiudadService {

    private CiudadRepository ciudadRepository;

    @Autowired
    @Qualifier(value="ciudadRepository")
    public void setCiudadRepository(CiudadRepository ciudadRepository) {
        this.ciudadRepository = ciudadRepository;
    }

    @Override
    public Iterable<Ciudad> listAllCiudad() {
        return ciudadRepository.findAll();
        //return  categoryRepository.findAll();
    }

    @Override
    public void saveCiudad(Ciudad ciudad) {
        ciudadRepository.save(ciudad);
    }

    @Override
    public Ciudad getCiudad(Integer id) {
        return ciudadRepository.findById(id).get();
        //return categoryRepository.findById(id).get();
    }

    @Override
    public void deleteCiudad(Integer id) {
        //categoryRepository.deleteById(id);;
        ciudadRepository.deleteById(id);
    }



}
