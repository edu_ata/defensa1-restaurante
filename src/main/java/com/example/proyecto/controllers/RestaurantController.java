package com.example.proyecto.controllers;
import com.example.proyecto.entities.LevelPrice;
import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.entities.Category;
import com.example.proyecto.services.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RestaurantController {
    private RestaurantService restaurantService;
    private CategoryService categoryService;
    private LevelPriceService levelPriceService;
    @Autowired
    public LevelPriceService getLevelPriceService() {
        return levelPriceService;
    }
    @Autowired
    public void setLevelPriceService(LevelPriceService levelPriceService) {
        this.levelPriceService = levelPriceService;
    }

    @Autowired
    public void setRestaurantService(RestaurantService productService){
     this.restaurantService=productService;
    }
    @Autowired
    public void setCategoryService(CategoryService categoryService){
        this.categoryService=categoryService;
    }

    @RequestMapping(value = "/restaurants", method = RequestMethod.GET)
    public String list(Model model) {
        Iterable<Restaurant> restaurantList=restaurantService.listAllRestaurants();
                model.addAttribute("variableTexto","Hello world");
                model.addAttribute("restaurantList",restaurantList);
                return "restaurants";
    }

    @RequestMapping(value= "/newRestaurant",method = RequestMethod.GET)
    String newRestaurant(Model model) {
        Iterable<Category> categories= categoryService.listAllCategorys();
        Iterable<LevelPrice> levelprices= levelPriceService.listAllLevelprice();

        model.addAttribute("categories",categories);
        model.addAttribute("levelprices",levelprices);

                return "newRestaurant";
    }

    @RequestMapping(value="/restaurant", method = RequestMethod.POST)
    String save(Restaurant restaurant) {
        restaurantService.saveRestaurant(restaurant);
        return "redirect:/restaurants";
    }

    @RequestMapping("/restaurant/{id}")
    String show(@PathVariable Integer id,Model model) {
        Restaurant restaurant=restaurantService.getRestaurant(id);
        model.addAttribute("restaurant",restaurant);
        return "show";
    }

    @RequestMapping("/editRestaurant/{id}")
    String editRestaurant(@PathVariable Integer id,Model model) {

        Restaurant restaurant=restaurantService.getRestaurant(id);
        model.addAttribute("restaurant",restaurant);
        Iterable<Category> categories=categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        return "editRestaurant";
    }

    @RequestMapping("/deleteRestaurant/{id}")
    String delete(@PathVariable Integer id)
    {
        restaurantService.deleteRestaurant(id);
               return "redirect:/restaurants";
    }


}
